// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

using System.Diagnostics.CodeAnalysis;

[assembly:
    SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames", Justification = "Test Project")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", 
        Target = "GastroPub.Service.Providers", Justification = "Test Project")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", 
        Target = "GastroPub.Service.RequestManagers", Justification = "Test Project")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
        Target = "GastroPub.Service.Providers.IAuthorizationProvider.#GetHeaders()", 
        Justification =
            "Using a method implies that there may be operations to generate the security headers, rather than static headers in all cases. This is more reusable."
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", 
        Target = "GastroPub.Service.Parsers", 
        Justification = "Test Project. A real project with same structure would have more members.")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
        Target = "GastroPub.Service.Providers.IConfigurationProvider.#GetServiceUri()", Justification = "Get operation")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", 
        Target = "GastroPub.Service.Unity", 
        Justification = "Unity has its own namespace as no existing namespace is relevant.")]
[assembly:
    SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", 
        Target = "GastroPub.Service.Unity.UnityConfig.#RegisterComponents()", 
        Justification = "Unity is required for dependency injection.")]
[assembly:
    SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", 
        Target =
            "GastroPub.Service.RequestManagers.ServiceRequestManager.#MakeGetRequest(System.Uri,System.Collections.Specialized.NameValueCollection)", 
        Justification =
            "Could not dispose, as the instance is required until response is consumed.Room for improvement here.")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", 
        Target = "GastroPub.Service.Models.SearchModels", 
        Justification = "The namespace is relevant to the operation performed.")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", 
        Target = "GastroPub.Service.ExtensionMethods", 
        Justification = "The namespace is relevant to the operation performed.")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", 
        Target = "GastroPub.Service.Exceptions", Justification = "The namespace is relevant to the operation performed.")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", 
        Target = "GastroPub.Service", Justification = "The namespace is relevant to the operation performed.")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "GastroPubServiceUri", 
        Scope = "member", Target = "GastroPub.Service.Providers.ConfigurationProvider.#GetServiceUri()", 
        Justification = "Using common convension for Uri naming.")]