﻿namespace GastroPub.Service.Parsers
{
    using System.Net.Http;
    using System.Threading.Tasks;

    using GastroPub.Service.Exceptions;

    using Newtonsoft.Json;

    public class HttpResponseMessageParser : IHttpResponseMessageParser
    {
        public async Task<T> Parse<T>(HttpResponseMessage httpResponseMessage)
        {
            T deserializedObject;
            try
            {
                var jsonData = await httpResponseMessage.Content.ReadAsStringAsync();
                deserializedObject = JsonConvert.DeserializeObject<T>(jsonData);
            }
            catch (JsonReaderException)
            {
                throw new ResponseNotParsableException(
                    string.Format(
                        "Status Code: {0} , Content: {1}", 
                        httpResponseMessage.StatusCode, 
                        httpResponseMessage.Content));
            }
            catch (JsonSerializationException)
            {
                throw new ResponseNotParsableException(
                    string.Format(
                        "Status Code: {0} , Content: {1}", 
                        httpResponseMessage.StatusCode, 
                        httpResponseMessage.Content));
            }

            return deserializedObject;
        }
    }
}