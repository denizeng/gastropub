namespace GastroPub.Service.Parsers
{
    using System.Net.Http;
    using System.Threading.Tasks;

    public interface IHttpResponseMessageParser
    {
        Task<T> Parse<T>(HttpResponseMessage httpResponseMessage);
    }
}