﻿namespace GastroPub.Service
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;

    using GastroPub.Service.Models.Responses;
    using GastroPub.Service.Models.SearchModels;
    using GastroPub.Service.Parsers;
    using GastroPub.Service.Providers;
    using GastroPub.Service.RequestManagers;
    using GastroPub.Service.Unity;

    using GastroPub.Service.Models;

    using Microsoft.Practices.Unity;

    public class RestaurantSearcher : IRestaurantSearcher
    {
        private readonly IAuthorizationProvider authorizationProvider;

        private readonly IConfigurationProvider configurationProvider;

        private readonly IServiceRequestManager requestManager;

        private readonly IHttpResponseMessageParser responseMessageParser;

        public RestaurantSearcher()
            : this(
                UnityConfig.Container.Resolve<IConfigurationProvider>(), 
                UnityConfig.Container.Resolve<IAuthorizationProvider>(), 
                UnityConfig.Container.Resolve<IServiceRequestManager>(), 
                UnityConfig.Container.Resolve<IHttpResponseMessageParser>())
        {
        }

        internal RestaurantSearcher(
            IConfigurationProvider configurationProvider, 
            IAuthorizationProvider authorizationProvider, 
            IServiceRequestManager requestManager, 
            IHttpResponseMessageParser responseMessageParser)
        {
            this.configurationProvider = configurationProvider;
            this.authorizationProvider = authorizationProvider;
            this.requestManager = requestManager;
            this.responseMessageParser = responseMessageParser;
        }

        public async Task<RestaurantSearchResponse> GetRestaurantResults(RestaurantSearch restaurantSearch)
        {
            var results = await this.GetRestaurantResultsByPostCode(restaurantSearch.Postcode);
            if (results.Restaurants != null)
            {
                results.Restaurants = results.Restaurants.Where(r => r.IsOpenNow.Equals(restaurantSearch.IsOpenNow));
            }

            return results;
        }

        internal async Task<RestaurantSearchResponse> GetRestaurantResultsByPostCode(string postcode)
        {
            var securityHeaders = this.authorizationProvider.GetHeaders();
            var serviceUri = this.configurationProvider.GetServiceUri();

            var queryParameters = HttpUtility.ParseQueryString(string.Empty);
            queryParameters.Add("q", postcode);

            var requestUri = new UriBuilder(serviceUri) { Query = queryParameters.ToString() };

            var headers = securityHeaders;
            headers.Add("Accept-Tenant", "uk");
            headers.Add("Accept-Language", "en-GB");

            var response = await this.requestManager.MakeGetRequest(requestUri.Uri, headers);
            var result = await this.responseMessageParser.Parse<RestaurantSearchResponse>(response);

            return result;
        }
    }
}