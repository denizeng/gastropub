﻿namespace GastroPub.Service.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ResponseNotParsableException : Exception
    {
        public ResponseNotParsableException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ResponseNotParsableException(string message)
            : base(message)
        {
        }

        public ResponseNotParsableException()
        {
        }

        protected ResponseNotParsableException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}