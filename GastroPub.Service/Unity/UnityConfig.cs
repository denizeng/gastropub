﻿namespace GastroPub.Service.Unity
{
    using System.Net.Http;

    using GastroPub.Service.Parsers;
    using GastroPub.Service.Providers;
    using GastroPub.Service.RequestManagers;

    using Microsoft.Practices.Unity;

    public static class UnityConfig
    {
        private static UnityContainer container;

        public static UnityContainer Container
        {
            get
            {
                return container ?? (container = RegisterComponents());
            }
        }

        public static UnityContainer RegisterComponents()
        {
            var unityContainer = new UnityContainer();

            unityContainer.RegisterType<HttpClient>(new InjectionConstructor());
            unityContainer.RegisterType<IHttpResponseMessageParser, HttpResponseMessageParser>();
            unityContainer.RegisterType<IAuthorizationProvider, AuthorizationProvider>();
            unityContainer.RegisterType<IConfigurationProvider, ConfigurationProvider>();
            unityContainer.RegisterType<IServiceRequestManager, ServiceRequestManager>();
            unityContainer.RegisterType<IRestaurantSearcher, RestaurantSearcher>();

            return unityContainer;
        }
    }
}