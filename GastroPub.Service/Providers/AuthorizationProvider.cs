﻿namespace GastroPub.Service.Providers
{
    using System.Collections.Specialized;

    public class AuthorizationProvider : IAuthorizationProvider
    {
        public NameValueCollection GetHeaders()
        {
            var authHeaders = new NameValueCollection();
            authHeaders.Add("Authorization", "Basic VGVjaFRlc3RBUEk6dXNlcjI=");
            return authHeaders;
        }
    }
}