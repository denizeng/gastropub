﻿namespace GastroPub.Service.Providers
{
    using System;

    public interface IConfigurationProvider
    {
        Uri GetServiceUri();
    }
}