namespace GastroPub.Service.Providers
{
    using System;
    using System.Configuration;

    public class ConfigurationProvider : IConfigurationProvider
    {
        public Uri GetServiceUri()
        {
            Uri serviceUri;

            try
            {
                serviceUri = new Uri(ConfigurationManager.AppSettings["GastroPubServiceUri"]);
            }
            catch (ArgumentNullException)
            {
                throw new ConfigurationErrorsException("GastroPubServiceUri key should be provided in configuration");
            }
            catch (UriFormatException)
            {
                throw new ConfigurationErrorsException("GastroPubServiceUri key should be a valid Uri");  
            }

            return serviceUri;
        }
    }
}