﻿namespace GastroPub.Service.Providers
{
    using System.Collections.Specialized;

    public interface IAuthorizationProvider
    {
        NameValueCollection GetHeaders();
    }
}