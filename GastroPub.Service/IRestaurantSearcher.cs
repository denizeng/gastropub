namespace GastroPub.Service
{
    using System.Threading.Tasks;

    using GastroPub.Service.Models.Responses;
    using GastroPub.Service.Models.SearchModels;

    using GastroPub.Service.Models;

    public interface IRestaurantSearcher
    {
        Task<RestaurantSearchResponse> GetRestaurantResults(RestaurantSearch restaurantSearch);
    }
}