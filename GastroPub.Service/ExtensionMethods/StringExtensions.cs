﻿namespace GastroPub.Service.ExtensionMethods
{
    using System.Collections.Generic;
    using System.Text;

    using GastroPub.Service.Models;

    public static class Extensions
    {
        public static string ToString(this IEnumerable<CuisineType> list, string separator)
        {
            var sb = new StringBuilder();
            if (list != null)
            {
                foreach (var element in list)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(separator);
                    }

                    sb.Append(element.Name);
                }
            }

            return sb.ToString();
        }
    }
}