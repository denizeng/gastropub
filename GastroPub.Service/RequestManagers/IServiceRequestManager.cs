namespace GastroPub.Service.RequestManagers
{
    using System;
    using System.Collections.Specialized;
    using System.Net.Http;
    using System.Threading.Tasks;

    public interface IServiceRequestManager
    {
        Task<HttpResponseMessage> MakeGetRequest(Uri requestUri, NameValueCollection headers);
    }
}