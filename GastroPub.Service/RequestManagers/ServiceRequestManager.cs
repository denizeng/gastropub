namespace GastroPub.Service.RequestManagers
{
    using System;
    using System.Collections.Specialized;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class ServiceRequestManager : IServiceRequestManager
    {
        private readonly HttpClient httpClient;

        public ServiceRequestManager(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public Task<HttpResponseMessage> MakeGetRequest(Uri requestUri, NameValueCollection headers)
        {
            var httpRequest = new HttpRequestMessage(HttpMethod.Get, requestUri);

            if (headers != null)
            {
                foreach (string headerName in headers)
                {
                    httpRequest.Headers.Add(headerName, headers[headerName]);
                }
            }

            var task = this.httpClient.SendAsync(httpRequest);
            return task;
        }
    }
}