﻿namespace GastroPub.Service.Models.Responses
{
    using System.Collections.Generic;

    public class RestaurantSearchResponse
    {
        public string ShortResultText { get; set; }

        public IEnumerable<Restaurant> Restaurants { get; set; }
    }
}