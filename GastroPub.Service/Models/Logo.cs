﻿namespace GastroPub.Service.Models
{
    using System;

    public class Logo
    {
        public Uri StandardResolutionURL { get; set; }
    }
}