﻿namespace GastroPub.Service.Models.SearchModels
{
    public class RestaurantSearch
    {
        public string Postcode { get; set; }

        public bool IsOpenNow { get; set; }
    }
}
