# Gastro Pub#

A simple app web app that can be used to display listings retrieved using a REST API.

It's unit/functional and integration tested.

Pending:
- Create a demo service to return results.
- Host Gastro Pub MVC app for demo access.