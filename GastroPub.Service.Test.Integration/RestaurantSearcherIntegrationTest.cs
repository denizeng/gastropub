﻿namespace GastroPub.Service.Test.Integration
{
    using System.Linq;
    using System.Threading.Tasks;

    using GastroPub.Service.Models.SearchModels;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Should;

    /// <summary>
    /// These tests will hit the real endpoint - there is no mocking done.
    /// </summary>
    [TestClass]
    public class RestaurantSearcherIntegrationTest
    {
        [TestMethod]
        public async Task SearchRestaurantByPostCode_WhenCalled_ReturnsSearchResults()
        {
            var searcher = new RestaurantSearcher();
            var searchRequest = new RestaurantSearch() { IsOpenNow = true, Postcode = "SE16" };
           
            var result = await searcher.GetRestaurantResults(searchRequest);

            result.Restaurants.Count().ShouldBeGreaterThan(0);
        }
    }
}