﻿namespace GastroPub.ConsoleApp
{
    using System;
    using System.Collections.Generic;

    using GastroPub.Service;
    using GastroPub.Service.Models;
    using GastroPub.Service.Models.SearchModels;

    using GastroPub.Service.ExtensionMethods;

    internal class Program
    {
        private static void Main(string[] args)
        {
            while (true)  
            {
                Console.WriteLine("Please enter letter q to exit, or type the postcode to search for open restaurants"); 
                string entry = Console.ReadLine();
                if (entry == "q")  
                {
                    break;
                }

                WriteSearchResultsForQuery(entry);
            }
        }

        private static void WriteSearchResultsForQuery(string postcode)
        {
            var restaurants = GetRestaurants(postcode);

            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("Search results for {0}", postcode);

            foreach (var restaurant in restaurants)
            {
                Console.WriteLine("--------------------------------------------");       
                Console.WriteLine("RestaurantName: {0}", restaurant.Name);
                Console.WriteLine("Number of Ratings: {0}", restaurant.NumberOfRatings);
                Console.WriteLine("Cuisine Types: {0}", restaurant.CuisineTypes.ToString(" & "));
                Console.WriteLine("--------------------------------------------");       
            }
            Console.WriteLine("--------------------------------------------");       
        }

        private static IEnumerable<Restaurant> GetRestaurants(string postcode)
        {
            RestaurantSearcher searcher = new RestaurantSearcher();
            var searchRequest = new RestaurantSearch() { IsOpenNow = true, Postcode = postcode };

            var result = searcher.GetRestaurantResults(searchRequest).Result;
            return result.Restaurants;
        }
    }
}