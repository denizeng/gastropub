﻿Feature: RestaurantSearch
	In order to find a restaurant in my area
	As a user
	I want to be able to enter a post code and get search results

@restaurantSearcher
Scenario: Search restaurants using post code
	Given I have navigated to search page
	And I have entered a post code into the search box
	When I press search
	Then the results should be shown
