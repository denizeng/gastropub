﻿namespace GastroPub.MvcApp.Test.Functional.Hooks
{
    using System.Reflection;

    using BoDi;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    using TechTalk.SpecFlow;

    [Binding]
    public class InitialisationHook
    {
        private readonly IObjectContainer objectContainer;

        public InitialisationHook(IObjectContainer objectContainer)
        {
            this.objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void InitialiseWebDriver()
        {
            var webDriver = new ChromeDriver(@"..\..\Dependencies");
            this.objectContainer.RegisterInstanceAs<IWebDriver>(webDriver);
        }

        [AfterScenario]
        public void CloseWebDriver()
        {
            var webDriver = this.objectContainer.Resolve<IWebDriver>();
            webDriver.Close();
        }
    }
}