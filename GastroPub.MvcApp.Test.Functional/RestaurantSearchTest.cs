﻿namespace GastroPub.MvcApp.Test.Functional
{
    using System;
    using System.Collections.ObjectModel;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Support.UI;

    using Should;

    /// <summary>
    /// To run functional tests, please make sure to start GastroPub.MvcApp first in a seperate Visual Studio, or the 
    /// same instance, but started using CTRL-F5
    /// </summary>
    [TestClass]
    public class RestaurantSearchTest
    {
        private IWebDriver webDriver;

        public RestaurantSearchTest()
        {
            this.webDriver = new ChromeDriver(@"Dependencies");  
        }

        [TestMethod]
        public void GivenIHaveNavigatedToGastroPubTest_WhenIEnterValidPostCode_IshouldBeShownAvailableRestaurants()
        {
            using (IWebDriver wdriver = this.webDriver)
            {
                wdriver.Navigate().GoToUrl("http://localhost:22000");
                wdriver.Manage().Window.Maximize();
                wdriver.FindElement(By.Id("postcodeTextBox")).SendKeys("SE16");
                wdriver.FindElement(By.Id("searchButton")).Click();

                WebDriverWait wait = new WebDriverWait(wdriver, TimeSpan.FromSeconds(10));
                ReadOnlyCollection<IWebElement> element = wait.Until(
                        ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.Id("content")));
                
                element[0].Displayed.ShouldBeTrue();
                wdriver.FindElements(By.CssSelector(".table tr")).Count.ShouldBeGreaterThan(0);
                wdriver.Quit();
            }
        }
    }
}