﻿namespace GastroPub.MvcApp.Test.Functional.Steps
{
    using System;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    using Should;

    using TechTalk.SpecFlow;

    [Binding]
    public sealed class RestaurantSearchSteps
    {
        private readonly IWebDriver webDriver;

        public RestaurantSearchSteps(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        [Given(@"I have navigated to search page")]
        public void GivenIHaveNavigatedToSearchPage()
        {
            this.webDriver.Navigate().GoToUrl("http://localhost:22000");
            this.webDriver.Manage().Window.Maximize();
        }

        [Given(@"I have entered a post code into the search box")]
        public void GivenIHaveEnteredAPostCodeIntoTheSearchBox()
        {
            this.webDriver.FindElement(By.Id("postcodeTextBox")).SendKeys("SE16");
        }

        [When(@"I press search")]
        public void WhenIPressSearch()
        {
            this.webDriver.FindElement(By.Id("searchButton")).Click();
        }

        [Then(@"the results should be shown")]
        public void ThenTheResultsShouldBeShown()
        {
            var wait = new WebDriverWait(this.webDriver, TimeSpan.FromSeconds(10));
            var element = wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.Id("content")));

            element[0].Displayed.ShouldBeTrue();
            this.webDriver.FindElements(By.CssSelector(".table tr")).Count.ShouldBeGreaterThan(0);
        }
    }
}