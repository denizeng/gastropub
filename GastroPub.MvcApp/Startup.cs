﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(GastroPub.MvcApp.Startup))]

namespace GastroPub.MvcApp
{
    using Owin;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
        }
    }
}