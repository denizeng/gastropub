﻿namespace GastroPub.MvcApp.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using GastroPub.MvcApp.Models;
    using GastroPub.Service;
    using GastroPub.Service.Models;
    using GastroPub.Service.Models.Responses;
    using GastroPub.Service.Models.SearchModels;

    using PagedList;

    public class HomeController : Controller
    {
        private readonly int pageSize = 10;

        private readonly IRestaurantSearcher restaurantSearcher;

        public HomeController(IRestaurantSearcher restaurantSearcher)
        {
            this.restaurantSearcher = restaurantSearcher;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return this.View();
        }

        [HttpGet]
        public async Task<PartialViewResult> GetSearchResults(SearchRequestModel searchRequestModel)
        {
            var restaurantSearchResponse = await this.GetRestaurants(searchRequestModel);
            var viewModel = await this.GetViewModel(searchRequestModel, restaurantSearchResponse);

            return this.PartialView("_SearchResults", viewModel);
        }

        private Task<SearchResultViewModel> GetViewModel(
            SearchRequestModel searchRequestModel, 
            RestaurantSearchResponse restaurantSearchResponse)
        {
            var currentPage = searchRequestModel.CurrentPage ?? 1;

            var pagedData =
                (PagedList<Restaurant>)restaurantSearchResponse.Restaurants.ToPagedList(currentPage, this.pageSize);

            var model = new SearchResultViewModel
                            {
                                Restaurants = pagedData, 
                                CurrentPage = currentPage, 
                                PageCount = pagedData.PageCount
                            };

            return Task.FromResult(model);
        }

        private async Task<RestaurantSearchResponse> GetRestaurants(SearchRequestModel searchRequestModel)
        {
            var restaurantSearch = new RestaurantSearch { IsOpenNow = true, Postcode = searchRequestModel.Postcode };
            return await this.restaurantSearcher.GetRestaurantResults(restaurantSearch);
        }
    }
}