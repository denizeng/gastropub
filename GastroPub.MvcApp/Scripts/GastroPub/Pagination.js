﻿'use strict';

var GastroPub = GastroPub || {};

(GastroPub.Pagination = function ($) {

    var hiddenSearchQuerySelector = '#hiddenSearchQuery',
        searchResultsContainerSelector = '#searchResultsContainer';

    var handlePageChange = function () {

        var clickedPageNumber = $(this).children('a').attr('data-page'),
            postCodeValue = $(hiddenSearchQuerySelector).val();

        window.GastroPub.Searcher.search({ Postcode: postCodeValue, CurrentPage: clickedPageNumber });
    };

    var init = function() {
        $(searchResultsContainerSelector).on("click", "li", handlePageChange);
    };

    $(function() {
        init();
    });

})(jQuery);