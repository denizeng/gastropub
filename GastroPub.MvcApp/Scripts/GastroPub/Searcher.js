﻿'use strict';

var GastroPub = GastroPub || {};

(GastroPub.Searcher = function($) {

    var searchUrl = '/Home/GetSearchResults',
        searchResultsContainer = '#searchResultsContainer',
        hiddenSearchQuery = '#hiddenSearchQuery',
        searchButton = '#searchButton',
        searchBox = '#postcodeTextBox';

    var handleSuccess = function (data) {
        $(searchResultsContainer).html(data);
    };

    var handleError = function () {
        alert('Failed to retrieve restaurants, please try again.');
    };

    var setCurrentQuery = function (postcode) {
        $(hiddenSearchQuery).val(postcode);
    };

    var buildRequest = function (currentPage, postcode) {
        var request = {
            CurrentPage: currentPage ? currentPage : 1,
            Postcode: postcode ? postcode : $(searchBox).val()
        };

        return request;
    };

    var validate = function(request) {
        if (request.Postcode === "") {
            $(searchBox).parent().addClass("has-error");
            return false;
        }

        $(searchBox).parent().removeClass("has-error");
        return true;
    };

    var search = function (request) {

        if (!request) {
            request = buildRequest();
            setCurrentQuery(request.Postcode);
        }

        if (!validate(request)) {
            return;
        }

        $.ajax({
            async: true,
            type: "GET",
            url: searchUrl,
            data: request,
            success: handleSuccess,
            error: handleError
        });
    };

    var handleSearch = function () {
        search();
    };

    var init = function () {
        $(searchButton).on("click", handleSearch);
        $(searchBox).on("keypress", function (event) {
            if (event.which === 13) {
                handleSearch();
            }
        });
    };

    $(function() {
        init();
    });

    return {
        search: search
    };

}(jQuery));