﻿namespace GastroPub.MvcApp.Models
{
    using Newtonsoft.Json;

    public class SearchRequestModel
    {
        public int? CurrentPage { get; set; }

        public string Postcode { get; set; }
    }
}