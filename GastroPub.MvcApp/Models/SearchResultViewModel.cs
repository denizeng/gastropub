﻿namespace GastroPub.MvcApp.Models
{
    using System.Collections.Generic;

    using GastroPub.Service.Models;

    public class SearchResultViewModel
    {
        public IEnumerable<Restaurant> Restaurants { get; set; }

        public int CurrentPage { get; set; }

        public int PageCount { get; set; }
    }
}