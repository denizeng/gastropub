﻿namespace GastroPub.Service.Test.Unit.Parsers
{
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading.Tasks;

    using GastroPub.Service.Exceptions;
    using GastroPub.Service.Models.Responses;
    using GastroPub.Service.Parsers;

    using GastroPub.Service.Models;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Ploeh.AutoFixture;

    using Should;

    [TestClass]
    public class HttpResponseMessageParserTest
    {
        private readonly HttpResponseMessageParser httpResponseMessageParser;

        public HttpResponseMessageParserTest()
        {
            this.httpResponseMessageParser = new HttpResponseMessageParser();
        }

        [TestMethod]
        public async Task Parse_WhenCalledWithValidResponse_ReturnsDeserialisedObject()
        {
            var fixture = new Fixture();
            var restaurantSearchResponse = fixture.Create<RestaurantSearchResponse>();

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponseMessage.Content = new ObjectContent<RestaurantSearchResponse>(
                restaurantSearchResponse, 
                new JsonMediaTypeFormatter(), 
                "application/json");

            var result = await this.httpResponseMessageParser.Parse<RestaurantSearchResponse>(httpResponseMessage);

            result.ShouldBeType(typeof(RestaurantSearchResponse));
        }

        [TestMethod]
        [ExpectedException(typeof(ResponseNotParsableException))]
        public async Task Parse_WhenCalledWithInvalidResponse_ThrowsRequestNotParsableException()
        {
            var invalidSearchResponse = "Error";

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponseMessage.Content = new ObjectContent<string>(
                invalidSearchResponse,
                new JsonMediaTypeFormatter(),
                "application/json");

            await this.httpResponseMessageParser.Parse<RestaurantSearchResponse>(httpResponseMessage);
        }
    }
}