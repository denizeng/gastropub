﻿namespace GastroPub.Service.Test.Unit
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;

    using GastroPub.Service.Models;
    using GastroPub.Service.Models.Responses;
    using GastroPub.Service.Parsers;
    using GastroPub.Service.Providers;
    using GastroPub.Service.RequestManagers;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Should;

    [TestClass]
    public class RestaurantSearcherTest
    {
        private readonly Mock<IAuthorizationProvider> mockAuthorizationProvider;

        private readonly Mock<IConfigurationProvider> mockConfigurationProvider;

        private readonly Mock<IHttpResponseMessageParser> mockHttpResponseMessageParser;

        private readonly Mock<IServiceRequestManager> mockServiceRequestManager;

        private readonly RestaurantSearcher restaurantSearcher;

        public RestaurantSearcherTest()
        {
            this.mockConfigurationProvider = new Mock<IConfigurationProvider>();
            this.mockConfigurationProvider.Setup(i => i.GetServiceUri()).Returns(new Uri("http://GastroPub.com"));

            var securityHeaders = new NameValueCollection();
            securityHeaders.Add("Authorization", "Basic abcdef");
            this.mockAuthorizationProvider = new Mock<IAuthorizationProvider>();
            this.mockAuthorizationProvider.Setup(i => i.GetHeaders()).Returns(securityHeaders);

            this.mockServiceRequestManager = new Mock<IServiceRequestManager>();
            this.mockServiceRequestManager.Setup(
                i => i.MakeGetRequest(It.IsAny<Uri>(), It.IsAny<NameValueCollection>()))
                .Returns(Task.FromResult(It.IsAny<HttpResponseMessage>()));

            this.mockHttpResponseMessageParser = new Mock<IHttpResponseMessageParser>();

            this.restaurantSearcher = new RestaurantSearcher(
                this.mockConfigurationProvider.Object, 
                this.mockAuthorizationProvider.Object, 
                this.mockServiceRequestManager.Object, 
                this.mockHttpResponseMessageParser.Object);
        }

        [TestMethod]
        public void GetRestaurantResultsByPostCode_WhenCalledWithAPostCode_ReturnsSearchResponse()
        {
            // Arrange
            var expected = new RestaurantSearchResponse();
            expected.Restaurants = new List<Restaurant> { new Restaurant { Address = "test" } };

            this.mockHttpResponseMessageParser.Setup(
                i => i.Parse<RestaurantSearchResponse>(It.IsAny<HttpResponseMessage>())).Returns(Task.FromResult(expected));

            // Act
            var actual = this.restaurantSearcher.GetRestaurantResultsByPostCode("test").Result;

            // Assert
            actual.Restaurants.Count().ShouldEqual(expected.Restaurants.Count());
        }
    }
}