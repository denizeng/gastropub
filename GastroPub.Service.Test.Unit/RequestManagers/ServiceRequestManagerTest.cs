﻿namespace GastroPub.Service.Test.Unit.RequestManagers
{
    using System;
    using System.Collections.Specialized;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading;
    using System.Threading.Tasks;

    using GastroPub.Service.Models.Responses;
    using GastroPub.Service.RequestManagers;

    using GastroPub.Service.Models;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;
    using Moq.Protected;

    using Should;

    [TestClass]
    public class ServiceRequestManagerTest
    {
        [TestMethod]
        public void MakeGetRequest_WhenCalledWithUriAndHeaders_SendsHttpGetRequestToClient()
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            responseMessage.Content = new ObjectContent<RestaurantSearchResponse>(
                new RestaurantSearchResponse(), 
                new JsonMediaTypeFormatter());
            var handler = new Mock<HttpMessageHandler>();

            handler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync", 
                    ItExpr.IsAny<HttpRequestMessage>(), 
                    ItExpr.IsAny<CancellationToken>())
                .Returns(
                    Task<HttpResponseMessage>.Factory.StartNew(
                        () => responseMessage));

            var mockHttpClient = new HttpClient(handler.Object);
            var serviceRequestManager = new ServiceRequestManager(mockHttpClient);

            var mockHeaders = new NameValueCollection();
            mockHeaders.Add("testheader", "testvalue");

            var mockRequestUri = new Uri("http://GastroPub.com");

            var mockRequest = new HttpRequestMessage(HttpMethod.Get, mockRequestUri);
            foreach (string mockHeader in mockHeaders)
            {
                mockRequest.Headers.Add(mockHeader, mockHeaders[mockHeader]);
            }

            var result = serviceRequestManager.MakeGetRequest(new Uri("http://GastroPub.com"), mockHeaders).Result;
            result.StatusCode.ShouldEqual(HttpStatusCode.OK);
        }
    }
}